-- PROCEDURE

-- Le but est de faire un filtre woofing pour une petite card

CREATE PROCEDURE `activity_woffing_for_card` ()
BEGIN

SELECT title,desciption,start_date,end_date FROM activity
WHERE woofing='true' AND activity_completed='false';

END

-- Le but est d'afficher le derniers inscrit 

CREATE PROCEDURE `last_register_5` ()

BEGIN

SELECT * FROM user ORDER BY Id DESC LIMIT 5

END


-- Le but est d'afficher par date un recherche d'activité

CREATE PROCEDURE `search_by_date` ( IN date_start DATE, IN date_end DATE)

BEGIN

    select title,description,volunters,start_date,end_date,woofing from activity where start_date >= date_start  and end_date <= date_end);
	
END

--

CREATE PROCEDURE `see_profil_for_one_page` ()

BEGIN

SELECT u.first_name, u.last_name, s.label,s.description FROM user u 
INNER JOIN skill  ON User_has_Skill.idUSer=User_has_Skill.idskill 
INNER JOIN address a ON User_has_address.idUser = User_has_address.id.idAddress;

END

-- 

CREATE PROCEDURE `count_last_activity_create_time_1H` ()

BEGIN

declare @hours date; 
set @hours = DATEADD(hour, -1, NOW()); 
SELECT Count(*) INTO activity  
WHERE hour < activity.creation_date;

END

-- Le but est de changer l'address par l'user connecté

CREATE PROCEDURE `change_address_by_user_connected` (IN idUser INT,IN street VARCHAR, IN street_number INT,IN city VARCHAR, IN country VARCHAR )

BEGIN

UPDATE address SET street= new.street ,street_number = new.street_number ,city = new.city ,country= new.country WHERE address.idUser = iduser ;


END

-- stat pour F/H

CREATE DEFINER=`root`@`localhost` PROCEDURE `mf_ratio`()

BEGIN
	
    DECLARE total_user INT;
    DECLARE total_f INT;
   
    SELECT COUNT(*) INTO total_user FROM user;
    SELECT COUNT(*) INTO total_f FROM user WHERE gender='F';
    
    SELECT total_f * 100 / total_employees AS ratio_mf;
	
    
END

-- pour creer une creer test pour le nouvelle utilisateur


CREATE DEFINER = CURRENT_USER TRIGGER `mydb`.`User_AFTER_INSERT` AFTER INSERT ON `User` FOR EACH ROW

BEGIN 

INSERT INTO activity(title,desciption,idUser) 
VALUES ('Bonjours voici votre premiere annonce','à vous de voir et tester',new-idUser); 

END



-- TRIGGER 

-- pour obliger les maj pour la ville

CREATE TRIGGER ville_en_majuscule
BEFORE INSERT ON address FOR EACH ROW SET NEW.city =UPPER(NEW.city);


-- pour faire un decompte des comptes sans avoir à faire un count all dans user

CREATE TRIGGER compte_+1_nombre_activity
BEFORE INSERT ON count_activity FOR EACH 
ROW update count_activity set point = point + 1 
WHERE activity.idActivity = new.idActivity;



